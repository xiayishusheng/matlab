function [imgDouble, imgGray, ...
    inThreshold, inImg2bin, imgIn, ...
    outThreshold, imgAdjust, outImg2bin, imgOut ] = ...
 FindInOut5( index, imgRaw )
    % convert to grayscale
    imgDouble = im2double(imgRaw);
    imgGray = rgb2gray(imgDouble);
    
    %% out
    % adjust img
    imgAdjust = imadjust(imgGray, [0.3 0.5], []);
    % get default threshold
    outThreshold = graythresh(imgAdjust);
    % convert to binary
    outImg2bin = im2bw(imgAdjust, outThreshold);
    % bin reverse
    outImg2binReverse = 1 - outImg2bin;
    % bin reverse fill
    outImg2binReverseFill = imfill(outImg2binReverse);
    imgOut = FindMaxArea(outImg2binReverseFill);
        
    %% in
    % get default threshold
    inThreshold = graythresh(imgGray);
    % convert to binary
    inImg2bin = im2bw(imgGray, inThreshold);
    imgIn = FindMaxEcc(inImg2bin);
    if(sum(imgIn(:)) >= sum(imgOut(:)))
        imgIn = FindMinEcc(inImg2bin);
    end
end
