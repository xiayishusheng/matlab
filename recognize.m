function recognize( method, targetDir )
    % save results to directory
    saveDir = fullfile(strcat(targetDir, '-save'), datestr(datetime, 'yyyy-mm-dd_HH-MM-SS'));
    if(~exist(saveDir, 'dir'))
        mkdir(saveDir);
    end

    % save format
    saveFormat = '.bmp';
    fnImgs = dir(fullfile(targetDir, '*.jpg'));
    excelFile = fullfile(saveDir, 'result.xlsx');
    excelData = [{'filename'}, {'inThreshold '}, {'outThreshold'}, {'inner_pixels'}, {'out_pixels'}];

    % deal images
    for index = 1:length(fnImgs)
        % read img
        imgRaw = imread(fullfile(targetDir, fnImgs(index).name));
        % Find in out area
        [imgDouble, imgGray, ...
            inThreshold, inImg2bin, imgIn, ...
            outThreshold, imgAdjust, outImg2bin, imgOut ] = method( index, imgRaw );
        
        % show images
        set(figure(1), 'name', targetDir, 'Numbertitle', 'off');
        subplot(331);imshow(imgDouble); title('imgDouble');
        subplot(332);imshow(imgGray);   title('imgGray');
        subplot(333);imshow(imgAdjust); title('imgAdjust');
        subplot(335);imshow(inImg2bin); title('inImg2bin');
        subplot(336);imshow(imgIn);     title('imgIn');
        subplot(338);imshow(outImg2bin);title('outImg2bin');
        subplot(339);imshow(imgOut);    title('imgOut');
        drawnow;pause(0.2);
        
        % save images
        filename = fnImgs(index).name(1:end - 4);
        imwrite(imgGray,    fullfile(saveDir, [filename '-1_imgGray'    saveFormat]));
        imwrite(inImg2bin,  fullfile(saveDir, [filename '-2_inImg2bin'  saveFormat]));
        imwrite(imgAdjust,  fullfile(saveDir, [filename '-3_imgAdjust'  saveFormat]));
        imwrite(outImg2bin, fullfile(saveDir, [filename '-4_outImg2bin' saveFormat]));
        imwrite(imgIn,      fullfile(saveDir, [filename '_in'  saveFormat]));
        imwrite(imgOut,     fullfile(saveDir, [filename '_out' saveFormat]));

        % save results
        rowData = [{fnImgs(index).name}, {inThreshold}, {outThreshold}, {sum(imgIn(:))}, {sum(imgOut(:))}];
        tempData = excelData;
        excelData = [tempData;rowData];
    end
    xlswrite(excelFile, excelData);
end
