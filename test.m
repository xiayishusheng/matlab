%% close all windows; clear variables; clear screen;
close all; clear; clc;

% I=imread('eight.tif');
% I=imread('peppers.png');
% I=imread('saturn.png');

%% test file
% imgRaw = imread('C:\temp\data-139k-0.6%aps-tol-4840k-500X-125-1\2019-03-19-139k-0.6%aps-tol-4840k-500X-125-0668.JPG');
% imgRaw = imread('C:\temp\data-139k-0.6%aps-tol-4840k-500X-125-1\2019-03-19-139k-0.6%aps-tol-4840k-500X-125-0548.JPG');
imgRaw = imread('C:\temp\data\20-80-130-1-2\0.8aps-4840k-tol-5.6k-chcl3-20-80-500r-200X-130-1-033.JPG');

[imgDouble, imgGray, ...
    inThreshold, inImg2bin, imgIn, ...
    outThreshold, imgAdjust, outImg2bin, imgOut ] = ...
FindInOut4(0, imgRaw);

subplot(331);imshow(imgDouble);  title('imgDouble');
subplot(332);imshow(imgGray);    title('imgGray');
subplot(334);imshow(inImg2bin);  title('inImg2bin');
subplot(335);imshow(imgIn);      title('imgIn');
subplot(337);imshow(imgAdjust);  title('imgAdjust');
subplot(338);imshow(outImg2bin); title('outImg2bin');
subplot(339);imshow(imgOut);     title('imgOut');
drawnow;pause(0.2);
