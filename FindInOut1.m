function [imgDouble, imgGray, ...
    inThreshold, inImg2bin, imgIn, ...
    outThreshold, imgAdjust, outImg2bin, imgOut ] = ...
FindInOut1( index, imgRaw )
    % convert to grayscale
    imgDouble = im2double(imgRaw);
    imgGray = rgb2gray(imgDouble);
    imgAdjust = imgGray;
    
    %% out
    % get default threshold
    outThreshold = graythresh(imgGray);
    % convert to binary
    outImg2bin = im2bw(imgGray, outThreshold);
    % bin reverse
    img2binReverse = 1 - outImg2bin;
    % bin reverse fill
    img2binReverseFill = imfill(img2binReverse);
    imgOut = FindMaxArea(img2binReverseFill);
    
    %% in
    inThreshold = outThreshold;
    inImg2bin = outImg2bin;
    imgIn = FindMaxEcc(inImg2bin);
    if(sum(imgIn(:)) >= sum(imgOut(:)))
        imgIn = FindMinEcc(inImg2bin);
    end
end
