function MinEcc = FindMinEcc(img)
    [labels, ~] = bwlabel(img);
    stats = regionprops(labels, 'all');
    Ars = cat(1, stats.Area);
    [ArSort, ISort] = sort(Ars, 'descend');
    ArSort(3:end) = [];
    maxEcc = inf;
    maxI = 0;
    for i = 1:length(ArSort)
        j = ISort(i);
        Ecc = stats(j).Eccentricity;
        if(Ecc <= maxEcc)
            maxEcc = Ecc;
            maxI = j;
        end
    end
    ITmp = labels == maxI;
    MinEcc = imfill(ITmp,'holes');
end
