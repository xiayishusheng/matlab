function [imgDouble, imgGray, ...
    inThreshold, inImg2bin, imgIn, ...
    outThreshold, imgAdjust, outImg2bin, imgOut ] = ...
    FindInOut6( ~, imgRaw )

% convert to grayscale
imgDouble = im2double(imgRaw);
imgGray = rgb2gray(imgDouble);
[height,width]=size(imgGray);
% imgAdjust = imgGray;
inThreshold=graythresh(imgGray);
inImg2bin=im2bw(imgGray,inThreshold);
[L,num]=bwlabel(inImg2bin, 4);
plot_x=zeros(1,num); %% position of the center of mass
plot_y=zeros(1,num);

for k=1:num
    sum_x=0;sum_y=0;area=0;
    for i=1:height
        for j=1:width
            if L(i,j)==k
                sum_x=sum_x+i;
                sum_y=sum_y+j;
                area=area+1;
            end
        end
    end
    plot_x(k)=fix(sum_x/area);
    plot_y(k)=fix(sum_y/area);
end

% figure(1);
% imshow(inImg2bin);

offset = 20; % offset
center_x = width/2;
center_y = height/2;

% find centroid
for i=1:num
    hold on
    p_y=plot_y(i);
    p_x=plot_x(i);
    if((p_x > center_x - offset && p_x < center_x + offset) && (p_y > center_y - offset && p_y < center_y + offset))
        plot(p_y ,p_x, '*');
        imgIn = L == i;
    end
end

outThreshold = 0;
imgAdjust = imgIn;
outImg2bin = imgIn;
imgOut = imgIn;

end
