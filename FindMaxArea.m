function MaxArea = FindMaxArea(img)
    [labels, ~] = bwlabel(img);
    stats = regionprops(labels, 'all');
    Ars = cat(1, stats.Area);
    try
        MaxArea = labels == find(Ars == max(Ars));
    catch
        MaxArea = 0;
    end
end
