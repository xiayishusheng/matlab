clear;clc;close all
I=imread('C:\temp\data\20-80-130-1-2\0.8aps-4840k-tol-5.6k-chcl3-20-80-500r-200X-130-1-033.JPG');
% I=imread('C:\Users\zhen\Desktop\1.jpg');
% I=imread('C:\Users\zhen\Desktop\2.jpg');

I_gray=rgb2gray(I);
level=graythresh(I_gray);

[height,width]=size(I_gray);
I_bw=im2bw(I_gray,level);

[L,num]=bwlabel(I_bw, 4);
plot_x=zeros(1,num); %% position of the center of mass
plot_y=zeros(1,num);

for k=1:num
    sum_x=0;sum_y=0;area=0;
    for i=1:height
        for j=1:width
            if L(i,j)==k
                sum_x=sum_x+i;
                sum_y=sum_y+j;
                area=area+1;
            end
        end
    end
    plot_x(k)=fix(sum_x/area);
    plot_y(k)=fix(sum_y/area);
end

figure(1);
imshow(I_bw);

center_x = width/2;
offset_x = width/10;
center_y = height/2;
offset_y = height/10;

for i=1:num
    hold on
    p_y=plot_y(i);
    p_x=plot_x(i);
    if((p_x>center_x-offset_x && p_x < center_x + offset_x) && (p_y>center_y-offset_y && p_y < center_y + offset_y))
        plot(p_x ,p_y, '*');
        ITmp = L == i;
        line([p_x,width], [p_y,0],'linestyle','-','color','r');
    end
end
