function [imgDouble, imgGray, ...
    inThreshold, inImg2bin, imgIn, ...
    outThreshold, imgAdjust, outImg2bin, imgOut ] = ...
FindInOut3( index, imgRaw )
    % convert to grayscale
    imgDouble = im2double(imgRaw);
    imgGray = rgb2gray(imgDouble);
    
    %% in
    % get default threshold
    inThreshold = graythresh(imgGray);
    % convert to binary
    inImg2bin = im2bw(imgGray, inThreshold);
    imgIn = FindMaxEcc(inImg2bin);
    
    %% out
    % adjust img
    imgAdjust = imadjust(imgGray, [0.3 0.6], []);
    % get default threshold
    outThreshold = graythresh(imgAdjust);
    outImg2bin = im2bw(imgAdjust, outThreshold - 0.2);
    imgOut = imfill(outImg2bin, 'holes');
end
