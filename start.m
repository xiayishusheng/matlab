%% close all windows; clear variables; clear screen;
close all; clear; clc;

%% 调用
% recognize(@FindInOut3, 'C:\temp\数据1\2017-04-08-1.5%aps-tol-PDMS-An-0-140dewetting-500X-1 1');
% recognize(@FindInOut1, 'C:\temp\数据2\2017-08-19-2.5%aps-ips=1-4-cb-200X-150-1');
% recognize(@FindInOut2, 'C:\temp\数据3\2017-06-25-2.0%aps-cb-500X-200-1');
% recognize(@FindInOut3, 'C:\temp\数据4\2017-06-25-1.7%aps-cb-500X-200-1');
% recognize(@FindInOut2, 'C:\temp\数据5\2017-06-26-2.0%aps-cb-140-An-2h-200dewetting-200X-1');
% recognize(@FindInOut3, 'C:\temp\数据6\2017-05-04-1.5%aps-tol-4000-150-500X-1');
% recognize(@FindInOut1, 'C:\temp\数据7\2.5%aps-cb-155℃-200X-1');

% recognize(@FindInOut1, 'C:\temp\matlab\2017-11-5-测试\2017-10-28-2.05%ips-aps=1-99-cb-560k-100X-200');
% recognize(@FindInOut1, 'C:\temp\matlab\2017-11-5-测试\2017-10-31-2.05%ips-aps=1-9-cb-560k-100X-200');

% recognize(@FindInOut4, 'C:\temp\data\data-139k-0.6%aps-tol-4840k-500X-125-1');
% recognize(@FindInOut5, 'C:\temp\data\2-36k-100-125-1');
% recognize(@FindInOut4, 'C:\temp\data\4-139k-100-air-125-1');

% % 圆环带和外部区域区分不明显，识别质心距离中心最近的内圆
% recognize(@FindInOut6, 'C:\temp\data\20-80-130-1-2');
% recognize(@FindInOut6, 'C:\temp\data\120-6');
% recognize(@FindInOut6, 'C:\temp\data\20-80-130-5-87');

% recognize(@FindInOut1, 'C:\temp\data\1.5%-200X-230-induces 425s');
