function [imgDouble, imgGray, ...
    inThreshold, inImg2bin, imgIn, ...
    outThreshold, imgAdjust, outImg2bin, imgOut ] = ...
FindInOut2( index, imgRaw )
    % convert to grayscale
    imgDouble = im2double(imgRaw);
    imgGray = rgb2gray(imgDouble);
    imgAdjust = imgGray;
    
    %% out
    % get default threshold
    outThreshold = graythresh(imgGray);
    % convert to binary
    outImg2bin = im2bw(imgGray, outThreshold);
    % bin reverse
    img2binReverse = 1 - outImg2bin;
    out = FindMaxArea(img2binReverse);
    imgOut = 1 - out;
    imgOut = FindMaxArea(imgOut);
    
    %% in
    inThreshold = outThreshold;
    inImg2bin = outImg2bin;
    imgIn = FindMaxEcc(inImg2bin);
    if(sum(imgIn(:)) / sum(imgOut(:)) > 0.98)
        imgIn = FindMinEcc(inImg2bin);
    end
end
