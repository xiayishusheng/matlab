# matlab

#### 项目简介
识别内外圆

#### 软件架构
使用matlab识别内外圆

#### 安装教程

1. 安装Matlab
2. 安装Excel

#### Excel加载项设置

1. 以**管理员方式**打开Excel -> 选项 -> COM加载项 -> 转到
![Excel选项-COM加载项](pics/1-Excel选项-COM加载项.png)
2. 勾掉加载项 -> 确定
![Excel选项-COM加载项](pics/2-勾掉全部加载项.png)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
